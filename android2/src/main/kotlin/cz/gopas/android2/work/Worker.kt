package cz.gopas.android2.work

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import kotlinx.coroutines.delay
import kotlin.time.Duration.Companion.seconds
import timber.log.Timber

class Worker(appContext: Context, params: WorkerParameters) : CoroutineWorker(appContext, params) {
    override suspend fun doWork(): Result {
        repeat(5) {
            Timber.d("Uploading...")
            delay(1.seconds)
        }
        return Result.success()
    }
}