package cz.gopas.android2.work

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.lifecycle.asFlow
import androidx.lifecycle.lifecycleScope
import androidx.work.Constraints
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.ExistingWorkPolicy
import androidx.work.NetworkType
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import cz.gopas.android2.BaseFragment
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import java.util.concurrent.TimeUnit

class WorkFragment : BaseFragment() {
    private val request = PeriodicWorkRequestBuilder<Worker>(1, TimeUnit.DAYS)
        .setConstraints(
            Constraints.Builder()
                .setRequiredNetworkType(NetworkType.UNMETERED)
                .setRequiresCharging(true)
                .setRequiresDeviceIdle(true)
                .build()
        )
        .build()

    private val workManager by lazy { WorkManager.getInstance(requireContext()) }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        workManager.enqueueUniquePeriodicWork(
            NAME,
            ExistingPeriodicWorkPolicy.UPDATE,
            request
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        workManager.getWorkInfosForUniqueWorkLiveData(NAME).asFlow().onEach {
            text.text = "${it.firstOrNull()}"
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private companion object {
        private const val NAME = "Upload"
    }
}
