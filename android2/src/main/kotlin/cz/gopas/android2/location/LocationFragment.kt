package cz.gopas.android2.location

import android.Manifest
import androidx.activity.result.contract.ActivityResultContracts
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.Priority
import cz.gopas.android2.BaseFragment
import timber.log.Timber

class LocationFragment : BaseFragment() {
    private val requestPermissionLauncher = registerForActivityResult(
        ActivityResultContracts.RequestPermission(),
    ) { isGranted: Boolean ->
        if (isGranted) {
            Timber.d("Permission granted")
            client.requestLocationUpdates(
                LocationRequest.Builder(Priority.PRIORITY_HIGH_ACCURACY, 1000).build(),
                locationCallback,
                null
            )
        } else {
            Timber.w("Permission refused")
        }
    }

    private val client by lazy {
        LocationServices.getFusedLocationProviderClient(requireContext())
    }

    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(loc: LocationResult) {
            text.text = "$loc"
        }
    }

    override fun onStart() {
        super.onStart()
        requestPermissionLauncher.launch(Manifest.permission.ACCESS_FINE_LOCATION)
    }

    override fun onStop() {
        super.onStop()
        client.removeLocationUpdates(locationCallback)
    }
}
