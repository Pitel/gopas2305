package cz.gopas.android2

import android.app.Application
import android.os.StrictMode
import com.google.android.material.color.DynamicColors
import timber.log.Timber

class App : Application() {
    init {
        StrictMode.setThreadPolicy(
            StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .build()
        )
        StrictMode.setVmPolicy(
            StrictMode.VmPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .build()
        )
        Timber.plant(Timber.DebugTree())
    }
    override fun onCreate() {
        super.onCreate()
//        DynamicColors.applyToActivitiesIfAvailable(this)
    }
}
