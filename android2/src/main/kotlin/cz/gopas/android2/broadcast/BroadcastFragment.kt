package cz.gopas.android2.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import cz.gopas.android2.BaseFragment
import timber.log.Timber

class BroadcastFragment : BaseFragment() {

    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            Timber.d("Battery update received")
            text.text = "${intent?.getIntExtra(BatteryManager.EXTRA_LEVEL, 0)}/${intent?.getIntExtra(BatteryManager.EXTRA_SCALE, 0)} ${intent?.getIntExtra(android.os.BatteryManager.EXTRA_PLUGGED, 999)}"
        }
    }

    override fun onStart() {
        super.onStart()
        requireContext().registerReceiver(receiver, IntentFilter(Intent.ACTION_BATTERY_CHANGED))
    }

    override fun onStop() {
        requireContext().unregisterReceiver(receiver)
        super.onStop()
    }
}
