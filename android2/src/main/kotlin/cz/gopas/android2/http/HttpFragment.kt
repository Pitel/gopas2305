package cz.gopas.android2.http

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import coil.load
import cz.gopas.android2.BaseFragment
import cz.gopas.android2.databinding.FragmentHttpBinding
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber

class HttpFragment : BaseFragment() {

    private var binding: FragmentHttpBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentHttpBinding.inflate(inflater, container, false)
        .also {
            binding = it
        }
        .root

    override fun onResume() {
        super.onResume()
        viewLifecycleOwner.lifecycleScope.launch {
            val ip = try {
                REST.ip()
            } catch (ce: CancellationException) {
                throw ce
            } catch(e: Exception) {
                Timber.w(e)
                null
            }
            Timber.d("$ip")
            binding?.text?.text = ip?.origin
        }
        binding?.image?.load("https://placekitten.com/500/500")
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }

    private companion object {
        private val REST = Retrofit.Builder()
            .baseUrl("https://httpbin.org")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(IpService::class.java)
    }
}
