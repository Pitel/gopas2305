package cz.gopas.android2.coroutines

import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import cz.gopas.android2.BaseFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

class CoroutinesFragment : BaseFragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Timber.d(requireContext().filesDir.canonicalPath) // Nedelat na main vlakne!

        val downloader = viewLifecycleOwner.lifecycleScope.async {
            downloadData()
        }

        repeat(10) {
            viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Default) {
                withContext(Dispatchers.Main) {
                    Timber.d(threadName)
                }
                withContext(Dispatchers.IO) {
                    Timber.d("IO $threadName")
                }
                Timber.d(threadName + downloader.await())
            }
        }
    }

    suspend fun downloadData() = "Hello"
}
