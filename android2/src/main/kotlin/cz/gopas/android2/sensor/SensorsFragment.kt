package cz.gopas.android2.sensor

import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import androidx.core.content.getSystemService
import cz.gopas.android2.BaseFragment
import timber.log.Timber

class SensorsFragment : BaseFragment() {
    private val sensorManager: SensorManager? by lazy { requireContext().getSystemService() }

    private val listener = object: SensorEventListener {
        override fun onSensorChanged(event: SensorEvent?) {
            Timber.v("${event?.values?.asList()}")
            text.text = "${event?.values?.asList()}"
        }

        override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
            Timber.d("Accuracy $accuracy")
        }
    }

    override fun onStart() {
        super.onStart()
        sensorManager?.getDefaultSensor(Sensor.TYPE_LIGHT)?.let { sensor ->
            sensorManager?.registerListener(listener, sensor, SensorManager.SENSOR_DELAY_UI)
        }
    }

    override fun onStop() {
        super.onStop()
        sensorManager?.unregisterListener(listener)
    }
}
