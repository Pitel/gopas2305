package cz.gopas.android2.camera

import android.content.ContentValues
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.lifecycleScope
import cz.gopas.android2.BaseFragment
import cz.gopas.android2.databinding.FragmentHttpBinding
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.File

class CameraFragment : BaseFragment() {

    private var binding: FragmentHttpBinding? = null

    private val camera = registerForActivityResult(
        ActivityResultContracts.TakePicture(),
    ) { captured: Boolean ->
        if (captured) {
            viewLifecycleOwner.lifecycleScope.launch {
                binding?.image?.setImageURI(uri.await())
            }
        } else {
            Timber.w("Image capture failed")
        }
    }

    private val uri = lifecycleScope.async(Dispatchers.IO, CoroutineStart.LAZY) {
        File.createTempFile("sjgfh", "dfasd").apply {
            deleteOnExit()
        }
        requireContext().contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, ContentValues())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentHttpBinding.inflate(inflater, container, false)
        .also { binding = it }
        .root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.setOnClickListener {
            lifecycleScope.launch(Dispatchers.IO) {
                camera.launch(uri.await())
            }
        }
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }
}
