package cz.gopas.android2.service

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import cz.gopas.android2.BaseFragment
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import timber.log.Timber

class ServiceFragment : BaseFragment() {

    private val service = MutableStateFlow<GopasService.GopasBinder?>(null)

    private val intent by lazy { Intent(requireContext(), GopasService::class.java) }

    private val conn = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            Timber.d("Service connected")
            this@ServiceFragment.service.value = service as GopasService.GopasBinder
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            Timber.d("Service disconnected")
            this@ServiceFragment.service.value = null
        }
    }

    override fun onStart() {
        super.onStart()

        service.filterNotNull().onEach {
            text.text = it.service.hello("Honza")
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        ContextCompat.startForegroundService(requireContext(), intent)
        requireContext().bindService(intent, conn, Context.BIND_AUTO_CREATE)
    }

    override fun onStop() {
        requireContext().unbindService(conn)
        requireContext().stopService(intent)
        service.value = null
        super.onStop()
    }
}
