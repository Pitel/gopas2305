package cz.gopas.android2.service

import android.app.Service
import android.content.Intent
import android.os.Binder
import androidx.core.app.NotificationChannelCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import timber.log.Timber

class GopasService : Service() {
    private val binder = GopasBinder()

    private val notificationManager by lazy { NotificationManagerCompat.from(this) }

    private val notification by lazy {
        NotificationCompat.Builder(this, GOPAS_CHANNEL_ID)
            .setSmallIcon(android.R.drawable.ic_media_play)
            .setContentTitle("Gopas")
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        Timber.d("Create")
        notificationManager.createNotificationChannel(
            NotificationChannelCompat.Builder(GOPAS_CHANNEL_ID, NotificationManagerCompat.IMPORTANCE_DEFAULT)
                .setName("Gopas")
                .build()
        )
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Timber.d("Start")
        startForeground(6546, notification)
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        Timber.d("Destroy")
        super.onDestroy()
    }

    fun hello(name: String) = "Hello $name"

    override fun onBind(intent: Intent?) = binder

    inner class GopasBinder : Binder() {
        val service = this@GopasService
    }

    private companion object {
        private const val GOPAS_CHANNEL_ID = "GOPAS_CHANNEL"
    }
}