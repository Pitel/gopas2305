package cz.gopas.kalkulacka

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupActionBarWithNavController
import cz.gopas.kalkulacka.about.AboutFragmentDirections
import cz.gopas.kalkulacka.about.Address
import cz.gopas.kalkulacka.about.User
import cz.gopas.kalkulacka.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }

    init {
        FragmentManager.enableDebugLogging(BuildConfig.DEBUG)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        Toast.makeText(this, "Create", Toast.LENGTH_SHORT).show()
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Create")
        }
        Log.d(TAG, "aaa")
        Log.d(TAG, getString(R.string.url))
        Log.d(TAG, "bbb")

        Log.d(TAG, "${intent.dataString}")

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.container) as NavHostFragment
        val navController = navHostFragment.navController
        setupActionBarWithNavController(navController)
    }

//    override fun onSaveInstanceState(outState: Bundle) {
//        super.onSaveInstanceState(outState)
//        outState.putCharSequence(RES_KEY, res.text)
//    }
//
//    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
//        super.onRestoreInstanceState(savedInstanceState)
//        res.text = savedInstanceState.getCharSequence(RES_KEY)
//    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.container)
        return navController.navigateUp() || super.onSupportNavigateUp()
    }

    override fun onResume() {
        super.onResume()
        Toast.makeText(this, "Resume", Toast.LENGTH_SHORT).show()
        Log.w(TAG, Exception("Resume"))
    }

    override fun onPause() {
        super.onPause()
        Toast.makeText(this, "Pause", Toast.LENGTH_SHORT).show()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.about -> {
            Log.d(TAG, "About clicked")
            findNavController(R.id.container).navigate(
                AboutFragmentDirections.about(
                    User(
                        "Honza Kaláb",
                        Address("Brno")
                    )
                )
            )
            true
        }

        else -> super.onOptionsItemSelected(item)
    }

    private companion object {
        private val TAG = MainActivity::class.simpleName
        private const val RES_KEY = "res"
    }
}