package cz.gopas.kalkulacka.calc

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Keep
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import cz.gopas.kalkulacka.R
import cz.gopas.kalkulacka.databinding.FragmentCalcBinding
import cz.gopas.kalkulacka.history.HistoryEntity
import cz.gopas.kalkulacka.history.HistoryViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class CalcFragment : Fragment() {
    private var binding : FragmentCalcBinding? = null
    private val viewModel: CalcViewModel by viewModels() // viewModels<CalcViewModel>()
    private val historyViewModel: HistoryViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentCalcBinding.inflate(inflater, container, false)
        .also {
            binding = it
        }
        .root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with (binding!!) {
            calc.setOnClickListener {
                calc()
            }
            share.setOnClickListener {
                share()
            }
            ans.setOnClickListener {
                aText.setText("${viewModel.ans}")
            }
            history.setOnClickListener {
                findNavController().navigate(CalcFragmentDirections.history())
            }
        }

//        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Default) {
//            historyViewModel.db.insert(HistoryEntity(1f), HistoryEntity(2f), HistoryEntity(3f))
//            historyViewModel.db.getAll().onEach {
//                Log.d(TAG, "$it")
//            }
//        }
    }

    override fun onStart() {
        super.onStart()
        viewModel.result.filterNotNull().onEach {
            binding?.res?.text = "$it"
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }

    private fun calc() {
        Log.d(TAG, "Calc")
        val a = binding?.aText?.text?.toString()?.toFloatOrNull() ?: Float.NaN
        val b = binding?.bText?.text?.toString()?.toFloatOrNull() ?: Float.NaN
        if (binding?.ops?.checkedRadioButtonId == R.id.div && b == 0f) {
            ZeroDialog().show(childFragmentManager, null)
        } else {
            viewModel.calc(a, b, binding!!.ops.checkedRadioButtonId)
        }
    }

    private fun share() {
        Log.d(TAG, "Share")
        val intent = Intent(Intent.ACTION_SEND)
            .putExtra(Intent.EXTRA_TEXT, getString(R.string.share_text, binding?.res?.text))
            .setType("text/plain")
        startActivity(intent)
    }

    private companion object {
        private val TAG = CalcFragment::class.simpleName
    }
}