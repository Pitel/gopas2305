package cz.gopas.kalkulacka.calc

import android.app.Application
import androidx.annotation.IdRes
import androidx.core.content.edit
import androidx.lifecycle.AndroidViewModel
import androidx.preference.PreferenceManager
import cz.gopas.kalkulacka.R
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlin.concurrent.thread
import kotlin.time.Duration.Companion.seconds

class CalcViewModel(app: Application) : AndroidViewModel(app) {
    private val _result = MutableStateFlow<Float?>(null)
    val result: StateFlow<Float?> = _result

    private val prefs = PreferenceManager.getDefaultSharedPreferences(getApplication())

    val ans: Float
        get() = prefs.getFloat(ANS_KEY, Float.NaN)

    fun calc(a: Float, b: Float, @IdRes op: Int) {
        thread { // Hardcore math!
            Thread.sleep(2.seconds.inWholeMilliseconds)
            _result.value = when (op) {
                R.id.add -> a + b
                R.id.sub -> a - b
                R.id.mul -> a * b
                R.id.div -> a / b
                else -> Float.NaN
            }.also {
                prefs.edit {
                    putFloat(ANS_KEY, it)
                }
            }
        }
    }

    private companion object {
        private const val ANS_KEY = "ans"
    }
}