package cz.gopas.kalkulacka.about

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import cz.gopas.kalkulacka.R

class AboutFragment : Fragment(R.layout.fragment_about) {

    private val args by navArgs<AboutFragmentArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (view as TextView).text = getString(R.string.about_text, "${args.user.name} von ${args.user.address.city}")
    }
}