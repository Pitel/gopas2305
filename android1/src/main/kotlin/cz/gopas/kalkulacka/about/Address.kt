package cz.gopas.kalkulacka.about

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Address(val city: String) : Parcelable