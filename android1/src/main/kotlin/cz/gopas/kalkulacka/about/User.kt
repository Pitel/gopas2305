package cz.gopas.kalkulacka.about

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class User(
    val name: String,
    val address: Address
) : Parcelable

