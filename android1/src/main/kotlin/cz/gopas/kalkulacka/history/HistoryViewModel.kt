package cz.gopas.kalkulacka.history

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.room.Room
import androidx.room.RoomDatabase
import kotlinx.coroutines.flow.MutableStateFlow

class HistoryViewModel(app: Application) : AndroidViewModel(app) {
    val db = Room.databaseBuilder(getApplication(), HistoryDatabase::class.java, "history")
//        .allowMainThreadQueries()
        .build()
        .historyDao()

    val selected = MutableStateFlow<Float?>(null)
}