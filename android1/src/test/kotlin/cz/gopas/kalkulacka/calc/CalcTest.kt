package cz.gopas.kalkulacka.calc

import cz.gopas.kalkulacka.R
import io.kotest.common.ExperimentalKotest
import io.kotest.core.spec.style.StringSpec
import io.kotest.framework.concurrency.eventually
import io.kotest.matchers.shouldBe
import io.mockk.clearAllMocks
import io.mockk.mockk
import kotlin.time.Duration.Companion.seconds

@OptIn(ExperimentalKotest::class)
class CalcTest : StringSpec({
    val viewModel = CalcViewModel(mockk(relaxed = true))

    "test" {
        viewModel.calc(1f, 1f, R.id.add)
        eventually(5.seconds) {
            viewModel.result.value shouldBe 2f
        }
    }

    afterTest {
        clearAllMocks()
    }
})