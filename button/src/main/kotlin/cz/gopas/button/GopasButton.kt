package cz.gopas.button

import android.content.Context
import android.util.AttributeSet
import com.google.android.material.R
import com.google.android.material.button.MaterialButton

class GopasButton @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = R.attr.materialButtonStyle
) : MaterialButton(context, attrs, defStyleAttr)